use serenity::framework::{
    standard::{macros::command, CommandResult, Args}
};
use serenity::model::prelude::*;
use serenity::prelude::*;


#[command]
#[required_permissions(ADMINISTRATOR)]
pub async fn mute(ctx: &Context, msg: &Message) -> CommandResult {
    let mention = msg.mentions.first().unwrap();
    let user_id = mention.id;

    if let Some(guild_id) = msg.guild_id {
        if let Ok(guild) = guild_id.to_partial_guild(&ctx).await {
            if let Ok(member) = guild.member(&ctx, user_id).await {
                if let Some(role) = guild.role_by_name("muted") {
                    if !member.roles.contains(&role.id) {
                        ctx.http.add_member_role(guild_id.0, user_id.0, role.id.0).await?;
                        msg.reply(&ctx, "Muted!").await?;
                    }
                }
            }
        }
    }

    Ok(())
}


#[command]
#[required_permissions(ADMINISTRATOR)]
pub async fn unmute(ctx: &Context, msg: &Message) -> CommandResult {
    let mention = msg.mentions.first().unwrap();
    let user_id = mention.id;

    if let Some(guild_id) = msg.guild_id {
        if let Ok(guild) = guild_id.to_partial_guild(&ctx).await {
            if let Ok(member) = guild.member(&ctx, user_id).await {
                if let Some(role) = guild.role_by_name("muted") {
                    if member.roles.contains(&role.id) {
                        ctx.http.remove_member_role(guild_id.0, user_id.0, role.id.0).await?;
                        msg.reply(&ctx, "Muted!").await?;
                    }
                }
            }
        }
    }

    Ok(())
}

#[command]
#[required_permissions(ADMINISTRATOR)]
pub async fn kick(ctx: &Context, msg: &Message) -> CommandResult {
    let mention = msg.mentions.first().unwrap();
    let guild = msg.guild(ctx.cache.as_ref()).await;
    let user_id = mention.id;

    ctx.http.kick_member_with_reason(guild.unwrap().id.0, user_id.0, "Kicked by nef, ask the owner for details").await?;
    msg.reply(&ctx, "Kicked!").await?;

    Ok(())
}

#[command]
#[required_permissions(ADMINISTRATOR)]
pub async fn ban(ctx: &Context, msg: &Message) -> CommandResult {
    let mention = msg.mentions.first().unwrap();
    let guild = msg.guild(ctx.cache.as_ref()).await;
    let user_id = mention.id;

    ctx.http.ban_user(guild.unwrap().id.0, user_id.0, 7, "Banned by nef, ask the owner for details").await?;
    msg.reply(&ctx, "Banned!").await?;

    Ok(())
}

#[command]
#[required_permissions(ADMINISTRATOR)]
pub async fn unban(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let current = args.current();
    let guild = msg.guild(ctx.cache.as_ref()).await;
    let user_id = current.unwrap().parse::<u64>().unwrap(); 

    ctx.http.remove_ban(guild.unwrap().id.0, user_id).await?;
    msg.reply(&ctx, "Unbanned!").await?;

    Ok(())
}

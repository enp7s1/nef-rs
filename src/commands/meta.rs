use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
pub async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Pong!").await?;

    Ok(())
}

#[command]
pub async fn avatar(ctx: &Context, msg: &Message) -> CommandResult {
    let mention = msg.mentions.first().unwrap();
    let avatar = mention.avatar.as_ref();
    msg.reply(ctx, avatar.unwrap()).await?;

    Ok(())
}

#[command]
pub async fn source(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "https://gitlab.com/enp7s1/nef-rs").await?;

    Ok(())
}
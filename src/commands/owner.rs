use serenity::framework::{
    standard::{macros::command, CommandResult, Args}
};
use serenity::model::prelude::*;
use serenity::prelude::*;

// CHANGE THESE VALUES
pub const ANNOUNCEMENTS_CHANNEL_ID: u64 = 897860864059793470; // change to your announcement channel id

// ill figure out this later, for now we're set
#[command]
#[owners_only]
pub async fn announce(ctx: &Context, _: &Message, args: Args) -> CommandResult {
    if let Some(channel) = ctx.cache.channel(ANNOUNCEMENTS_CHANNEL_ID).await {
        let guild = channel.guild().unwrap();
        let current = args.rest();

        guild.send_message(&ctx.http, |m| {
            m.content(format!("@everyone\n\n{}", current));

            m
        }).await?;
    }

    Ok(())
}
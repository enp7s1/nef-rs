mod commands;

use serenity::{
    async_trait,
    model::{gateway::Ready},
    http::Http
};
use serenity::client::{Client, Context, EventHandler};
use serenity::model::channel::Message;
use serenity::framework::standard::{
    StandardFramework,
    CommandResult,
    macros::{
        command,
        group
    }
};

use std::env;
use commands::{meta::*, owner::*, admin::*};
use std::{collections::HashSet};

#[group]
#[commands(help, ping, avatar, source, mute, unmute, kick, ban, unban, announce)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        // no racism intended, its for moderation purposes. You can add more if you want. This is not the most efficient check for this type of stuff
        if msg.content.to_lowercase().contains("nigger") || msg.content.to_lowercase().contains("nigga") {
            println!("Found a racist message that was sent by {}#{}", msg.author.name, msg.author.discriminator);
            let _ = msg.delete(&ctx.http).await;
            // we dont care about the return thing
            let _dm = msg.author.dm(&ctx.http, |m| {
                m.content("Hey you, dont send racist messages okay?");
                m
            }).await;
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{}#{} is connected!", ready.user.name, ready.user.discriminator);
    }
}

#[tokio::main]
async fn main() {
    // get owners
    let token = env::var("DISCORD_TOKEN").expect("token");
    let http = Http::new_with_token(&token);

    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let framework = StandardFramework::new()
        .configure(|c| c.owners(owners).prefix("!")) // set the bot's prefix to "!"
        .group(&GENERAL_GROUP);

    // Login with a bot token from the environment
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, HELP_STR).await?;

    Ok(())
}

const HELP_STR: &str = r#"Hello! These are the current commands:
**DISCLAIMER** messages might be slow since this is using <https://github.com/serenity-rs/serenity> and is in beta currently.

1. help - sends this message
2. ping - replies with pong
3. avatar - sends the avatar string
4. source - sends the gitlab link to the bot's source

**Owner(s) commands**
1. mute - mute a mentioned user
2. unmute - unmute a mentioned user
3. kick - kick a mentioned user
4. ban - bans a mentioned user
5. unban - unbans a mentioned user
6. announce - send an announcement to the announcement channel
"#;

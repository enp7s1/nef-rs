# nef-rs
Moderation discord bot

# Starting the bot

## Setting up the discord token

### Linux / Darwin
You can run this line of code in your terminal, replace `TOKEN_HERE` with your bot's discord token.
```bash
export DISCORD_TOKEN=TOKEN_HERE
```

### Windows 10 or lower
You can run this line inside PowerShell or CMD, replace `TOKEN_HERE` with your bot's discord token.
```bat
set DISCORD_TOKEN=TOKEN_HERE
```

## Building the bot
You will have to run `cargo build --release` to build it, after that's done you can start running your but with `./target/release/nef`, this will work on all known operating systems

## Changing default values
This is needed for the bot to function properly, the current values for roles, the guild id and channels. You can edit these values [here](src/commands/owner.rs) at the `// CHANGE THESE VALUES` comment

# Contributing new commands
Yes! You can add new commands if you do the work yourself, its open-source for a reason!

## Clone the repository
Run `git clone https://gitlab.com/enp7s1/nef-rs.git` to clone it, after that's done cd into the directory.

## Make your changes
You can find all the commands in [here](src/commands/), create a command file and add it to [mod.rs](src/commands/mod.rs)

## Checkout a new branch
Checkout also known as creating a new branch is used for identifying multiple updates to the same project, you can checkout a new branch with `git checkout -b branch_name`. Replace `branch_name` with the name of your feature(s), example: `git checkout -b avatar-command`

## Creating a new remote for the upstream repository
You can do this by executing this in your terminal:
```
git remote add upstream https://gitlab.com/enp7s1/nef-rs.git
```

## Adding your files to git
This is needed else your changes wont appear, you can do this by executing `git add .` for all the files you changed / added.

## Adding a commit message
Finally, after all that hassle of writing code, testing it, fixing errors, etc you can finally push to your branch but not before you set a commit message! You can do that by executing this line `git commit -S -m "message_here"`, replace `message_here` with your message of choice.

## Pushing to the repository
Ah finally, you've done all of the things needed for committing a new command / feature, now its time to push it! You can execute `git push -u origin branch_name` in your terminal, don't forget to change `branch_name` to your branch name.

## Creating the pull request
Navigate to [Merge Requests](https://gitlab.com/enp7s1/nef-rs/-/merge_requests) and create a merge request! You can find more information about this [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)